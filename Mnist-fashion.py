import tensorflow as tf
from tensorflow import keras

from CustomCallBack import CustomCallBack

print(tf.__version__)

callback = CustomCallBack()

#
fashion_mnist = keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

training_images = train_images / 255.0
testing_images = test_images / 255.0

print(train_images)

# Setting up the model
model = keras.Sequential([keras.layers.Flatten(), keras.layers.Dense(64, activation=tf.nn.relu),
                          keras.layers.Dense(10, activation=tf.nn.softmax)])

# Giving Compile Options
model.compile(optimizer=tf.optimizers.Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Fitting the model
model.fit(training_images, train_labels, epochs=10, callbacks=[callback])

print('Evaluating The Accuracy')
model.evaluate(testing_images, test_labels)
