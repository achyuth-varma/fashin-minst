from tensorflow import keras


class CustomCallBack(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if (logs.get('accuracy') >= 0.9):
            print("\n There is a high accuracy")
            self.model.stop_training = True
