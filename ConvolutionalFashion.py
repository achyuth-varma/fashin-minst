import tensorflow as tf
from tensorflow import keras

from CustomCallBack import CustomCallBack

print(tf.__version__)

callback = CustomCallBack()

#
fashion_mnist = keras.datasets.fashion_mnist

(training_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

training_images = training_images.reshape(60000, 28, 28, 1)
training_images = training_images / 255.0
test_images = test_images.reshape(10000, 28, 28, 1)
test_images = test_images / 255.0

# Setting up the model
model = keras.Sequential([
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=(28, 28, 1)),
    tf.keras.layers.MaxPooling2D(2, 2),
    keras.layers.Flatten(), keras.layers.Dense(64, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)])

# Giving Compile Options
model.compile(optimizer=tf.optimizers.Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Fitting the model
model.fit(training_images, train_labels, epochs=10, callbacks=[callback])

print('Evaluating The Accuracy')
model.evaluate(testing_images, test_labels)
